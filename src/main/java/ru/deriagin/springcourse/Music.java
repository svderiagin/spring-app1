package ru.deriagin.springcourse;

import java.util.List;

public interface Music {
    String getSong();
}
